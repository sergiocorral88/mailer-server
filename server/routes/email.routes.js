module.exports = app => {

    const emailController = require('../controllers/email.controller');

    const router = require("express").Router();

    router.post('/send', emailController.sendEmailToValidate);

    app.use('/api/email', router);
};