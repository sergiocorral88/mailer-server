"use strict";
const nodemailer = require("nodemailer");

/** This function configures the service and sends the email with the data provided */
const createAndSendEmail = async(from, to, subject, text, html, attachments) => {
    let transporter;

    /** Configure the service for development or production */
    if (process.env.NODE_ENV === 'dev') {
        // Generate test SMTP service account from ethereal.email
        const testAccount = await nodemailer.createTestAccount();

        // create reusable transporter object using the default SMTP transport
        transporter = nodemailer.createTransport({
            host: "smtp.ethereal.email",
            port: 587,
            secure: false, // true for 465, false for other ports
            auth: {
                user: testAccount.user, // generated ethereal user
                pass: testAccount.pass, // generated ethereal password
            },
        });
    } else {
        // create reusable transporter object with the PRODUCTION CONFIG.
        transporter = await nodemailer.createTransport(process.env.SMTP_URL);
    }

    // send mail with defined transport object
    const info = await transporter.sendMail({
        from, // sender address
        to, // list of receivers
        subject, // Subject line
        text, // plain text body
        html, // html body
        attachments // Using Embedded Images
    });

    // ONLY FOR TEST: shows a url to see the email sent. it is only for the purpose of visualizing the process.
    if (process.env.NODE_ENV === 'dev') {
        console.log("Message sent: %s", info.messageId);
        // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

        // Preview only available when sending through an Ethereal account
        console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
        // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
    }

}

module.exports = {
    createAndSendEmail
};