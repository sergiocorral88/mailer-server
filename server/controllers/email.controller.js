"use strict";
const emailValidator = require('email-validator');
const emailService = require('../services/email.service');

/** Send a validation email to an address passed by parameter. */
const sendEmailToValidate = async(req, res) => {
    /** Get the email or null */
    const to = req.body.emailTo || null;

    /** Validates the email parameter and returns an error if it does not exists or is invalid */
    if (!to) {
        return res.status(400).json({
            ok: false,
            err: {
                message: 'emailTo is required.'
            }
        });
    } else if (!emailValidator.validate(to)) {
        return res.status(400).json({
            ok: false,
            err: {
                message: `The email (${to}) is invalid.`
            }
        });
    }

    /** The necessary data to send the email was configured */
    const from = 'pedrogar888@gmail.com';
    const subject = 'Account validation by Sergio Corral';
    const text = null;
    const html = { path: 'public/templates/account-validation/account-validation.html' };
    const attachments = [{
        filename: 'facebook.png',
        path: 'public/templates/account-validation/images/facebook.png',
        cid: 'facebook' // Same cid value as in the html img src
    }, {
        filename: 'twitter.png',
        path: 'public/templates/account-validation/images/twitter.png',
        cid: 'twitter'
    }, {
        filename: 'linkedin.png',
        path: 'public/templates/account-validation/images/linkedin.png',
        cid: 'linkedin'
    }, {
        filename: 'youtube.png',
        path: 'public/templates/account-validation/images/youtube.png',
        cid: 'youtube'
    }, {
        filename: 'google2.png',
        path: 'public/templates/account-validation/images/google2.png',
        cid: 'google'
    }, {
        filename: 'logo.png',
        path: 'public/templates/account-validation/images/logo.png',
        cid: 'logo'
    }];

    /** Call the function in charge of sending the email */
    emailService.createAndSendEmail(from, to, subject, null, html, attachments)
        .then(() => {
            return res.json({
                ok: true,
                message: 'The email was sent.'
            });
        })
        .catch((errs) => {
            return res.status(500).json({
                ok: false,
                err: {
                    message: 'An error occurred while sending the email.'
                },

            });
        });
};

module.exports = {
    sendEmailToValidate
};