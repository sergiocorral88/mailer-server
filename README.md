# About the project

API Rest email server with NodeJS and Express, that sends an account validation template to the entered email.

## Email template

![Alt text](./public/templates/account-validation/images/template.png?raw=true "Template Email")

## Usage

### Local

Run in project directory

`npm install`

and

`npm run nodemon`

then go to

http://localhost:3000

### Remote

Go to

https://mailer-server-nodejs.herokuapp.com/


## Routes
### `POST`
#### Local
http://localhost:3000/api/email/send

#### Remoto
https://mailer-server-nodejs.herokuapp.com//api/email/send


#### body

{
    emailTo: < email >
}

Replace the < email > with the destination.

## Test

### Postman

![Alt text](./public/templates/account-validation/images/test-send-email-postman.png?raw=true "Test with Postman")



## Used packages

### express

Fast, unopinionated, minimalist web framework for node.

https://www.npmjs.com/package/express

### nodemailer

Nodemailer is a module for Node.js applications to allow easy as cake email sending.

https://www.npmjs.com/package/nodemailer

### email-validator

A module to validate an e-mail address

https://www.npmjs.com/package/email-validator

### body-parser

Node.js body parsing middleware.

Parse incoming request bodies in a middleware before your handlers, available under the req.body property.

https://www.npmjs.com/package/body-parser

### Nodemon

Nodemon is a tool that helps develop node.js based applications by automatically restarting the node application when file changes in the directory are detected.

https://www.npmjs.com/package/nodemon

